function addTokens(input, tokens){
    // check if input is actually a string
    if (typeof input !== 'string') {
        throw new Error('Invalid input');
    }

    // check if input has at least 6 characters
    if (input.length  < 6) {
        throw new Error('Input should have at least 6 characters');
    }

    //check if all the tokens have the correct format
    if (tokens.filter(token => token.tokenName === undefined || typeof token.tokenName !== 'string').length > 0) {
        throw new Error('Invalid array format');
    }

    // check if there is anything that needs to be replaced
    if (!input.includes('...')) {
        return input;
    } else {
        let words = input.split(' ');
        let tokenIndex = 0;
        for (let i = 0; i < words.length; ++i) {
            if (words[i].includes('...')) {
                words[i] = words[i].replace('...', `\${${tokens[tokenIndex].tokenName}}`)
                ++tokenIndex;
            }
        }
        let newInput = "";
        for (let i = 0; i < words.length; ++i) {
            newInput += words[i] + (i < words.length - 1 ? " " : "");
        }
        // console.log(newInput);
        return newInput;
    }

}

const app = {
    addTokens: addTokens
}

module.exports = app;

let input = 'Subsemnatul ..., dominiciliat in ...';
let token1 = { tokenName: 'Eu' };

let token2 = { tokenName: 'Planeta Romanica' };

let tokens = [];
tokens.push(token1);
tokens.push(token2);

// console.log(tokens[1].tokenName);


console.log(addTokens(input, tokens));